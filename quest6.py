import dill
import math
####################################################################################################
class row:
    word = ""
    freq = 1
    tf = 0
    idf = 0
    tf_idf = 0
    pointers = []

#------------------------------------------------#
    def __init__(self,word="",freq=1,tf=0):
        self.word = word
        self.freq = freq
        self.tf = tf
        self.idf = 0
        self.tf_idf = 0
        self.pointers = []

####################################################################################################
class bag:
    docNum = 0
    rows = []

#------------------------------------------------#
    def __init__(self,docNum,lista):
        self.docNum = docNum
        self.rows = []

        words = []

        for item in lista:
            if words.__contains__(item) == 1:
                self.increase_row_freq(item)

            else:
                words.append(item)
                words.sort()
                order = words.index(item)
                self.rows.insert(order,row(item))

        self.calc_tf()

#------------------------------------------------#
    def find_row(self,word):
        for item in self.rows:
            if item.word == word:
                return item
        return row

#------------------------------------------------#
    def increase_row_freq(self,word):
        row = self.find_row(word)
        row.freq += 1

#------------------------------------------------#
    def calc_tf(self):
        for row in self.rows:
            row.tf = row.freq / self.rows.__len__()

#------------------------------------------------#
    def calc_tf_idf(self,idfDict):
        for row in self.rows:
            print("For BOW: "+str(self.docNum)+", Calculating TF-IDF: "+str(row.word))
            try:
                row.tf_idf = row.tf * idfDict[row.word]
            except:
                row.tf_idf = 0

#------------------------------------------------#
    def write_bag_file(self):
        dill.dump(self, file = open("utilities/bags/pickle/"+str(self.docNum)+".pickle", "wb"))
        file = open("utilities/bags/txt/"+str(self.docNum)+".txt", "w")
        for row in self.rows:
            file.writelines(row.word+" | "+str(row.freq)+" | "+str(row.tf)+" | "+str(row.tf_idf)+"\n")
        file.close()

#------------------------------------------------#
    def print_bag(self):
        print("Doc | Word  | Frequency | TF | TF-IDF")
        for row in self.rows:
            print(row.word+" | "+str(row.freq)+" | "+str(row.tf)+" | "+str(row.tf_idf))

####################################################################################################
class index:
    rows = []
    idfDict = dict.fromkeys([''],0)

#------------------------------------------------#
    def __init__(self,bagsList):
        self.rows = []
        self.idfDict = dict.fromkeys([''],0)

        words = []

        i = 1
        for bag in bagsList:
            print('Merging: '+str(i))
            i += 1
            for innerRow in bag.rows:
                if words.__contains__(innerRow.word):
                    self.edit_row(innerRow.word, innerRow.freq, bag.docNum)
                else:
                    words.append(innerRow.word)
                    words.sort()
                    order = words.index(innerRow.word)
                    newRow = row(innerRow.word,innerRow.freq)
                    newRow.pointers.append(bag.docNum)
                    self.rows.insert(order,newRow)

        self.calc_idf(words,bagsList.__len__())

#------------------------------------------------#
    def find_row(self,word):
        for item in self.rows:
            if item.word == word:
                return item
        return row

#------------------------------------------------#
    def edit_row(self,word,freq,docNum):
        row = self.find_row(word)
        row.freq += freq
        if row.pointers.__contains__(docNum) == 0:
            row.pointers.append(docNum)

#------------------------------------------------#
    def calc_idf(self,wordsList,corpusCount):
        self.idfDict=dict.fromkeys(wordsList,0)

        for row in self.rows:
            print("Calculating IDF: "+row.word)
            row.idf = math.log(corpusCount/float(row.freq))
            self.idfDict[row.word] = row.idf

#------------------------------------------------#
    def print_index(self):
        print("Word | Frequency | IDF | Pointers")
        for row in self.rows:
            print(row.word+" | "+str(row.freq)+" | "+str(row.idf)+" | "+str(row.pointers))  

#------------------------------------------------#
    def write_index_file(self):
        dill.dump(self, file = open("utilities/index.pickle", "wb"))
        file = open("utilities/index.txt", "w")
        for row in self.rows:
            file.writelines(row.word+" | "+str(row.freq)+" | "+str(row.idf)+" | "+str(row.pointers)+"\n")
        file.close()

####################################################################################################
def read_bag(docNum):
    return dill.load(open("utilities/bags/pickle/"+str(docNum)+".pickle", "rb"))

def read_index():
    return dill.load(open("utilities/index.pickle", "rb"))