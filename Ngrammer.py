def split_word_into_ngrams_letters(word, n):

    # Break word into tokens
    tokens = [token for token in word]

    # generate ngram using zip
    ngrams = zip(*[tokens[i:] for i in range(n)])

    # concat with empty space
    lista=[''.join(ngram) for ngram in ngrams]

    return lista

####################################################################################################
def split_word_into_ngrams_letters_by_range(word,n,n1):

    lista = []

    for nlen in range(n,n1):
        for ii in range(len(word)-nlen+1):
            lista.append(word[ii:(ii+nlen)])
    
    return lista

####################################################################################################
from nltk import ngrams
def split_sentence_into_ngrams_words(sentence,n):

    sixgrams = ngrams(sentence.split(), n)

    lista = []

    for grams in sixgrams:
        lista.append(grams)

    return lista
