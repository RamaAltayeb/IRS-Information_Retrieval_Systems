import json
####################################################################################################
def read_file(filePath):
    file = open(filePath, "r")
    content = file.read()
    file.close()
    return content
    
####################################################################################################
def read_json(filePath):
    file = open(filePath)
    data = json.load(file)
    file.close()
    return data