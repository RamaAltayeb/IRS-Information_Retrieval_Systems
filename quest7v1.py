import math
import quest6 as q6
####################################################################################################
def find_matching_corpus(query,index):
    matchingFiles = []

    for row in index.rows:
        if query.__contains__(row.word) == 1:
            for fileNum in row.pointers:
                if matchingFiles.__contains__(fileNum) == 0:
                    matchingFiles.append(fileNum)

    return matchingFiles

####################################################################################################
def build_vectors(query,matchingFiles):
    queryBag = q6.bag(0,query)
    queryVector = []
    
    for row in queryBag.rows:
        queryVector.append(row.freq)

    bagsVectors = []

    for fileNum in matchingFiles:
        bag = q6.read_bag(fileNum)
        vector = []

        for word in query:
            bagHasWord = 0
            bagWordFreq = 0
            for row in bag.rows:
                if row.word == word:
                    bagHasWord = 1
                    bagWordFreq = row.freq
            vector.append(bagWordFreq)

        bagsVectors.append(vector)
    
    return queryVector,bagsVectors

####################################################################################################
def filter_corpus_by_cos(queryVector,bagsVectors,matchingFiles):
    cosDict=dict.fromkeys('',0)
    result = []

    denominator_part2 = 0
    for i in range(0,queryVector.__len__()):
        denominator_part2 += queryVector.__getitem__(i) ^ 2

    for j in range(0,bagsVectors.__len__()):
        numerator = 0
        denominator = 0
        denominator_part1 = 0
        for i in range(0,queryVector.__len__()):
            numerator += bagsVectors.__getitem__(j).__getitem__(i) * queryVector.__getitem__(i)
            denominator_part1 += bagsVectors.__getitem__(j).__getitem__(i) ^ 2
        denominator =math.sqrt(denominator_part1 * denominator_part2)
        fraction = 0
        if denominator != 0:
            fraction = numerator / denominator
        if fraction >= 0.81:
            cosDict.update({matchingFiles.__getitem__(j):fraction})
            # result.append(matchingFiles.__getitem__(j))
    
    cosDict = dict(sorted(cosDict.items(), key=lambda item: item[1], reverse = True))
    print(cosDict)

    result = list(cosDict.keys())[:10] 
    return result

####################################################################################################
def match(query,index):
    matchingFiles = find_matching_corpus(query,index)
    queryVector,bagsVectors = build_vectors(query,matchingFiles)
    return filter_corpus_by_cos(queryVector,bagsVectors,matchingFiles)