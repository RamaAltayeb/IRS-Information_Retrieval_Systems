import quest1 as q1
import quest2 as q2
import quest3 as q3
import quest4 as q4
import quest5 as q5
import quest6 as q6
import quest7v1 as q7v1
import quest7v2 as q7v2
import quest8 as q8
import FilesReader as reader
####################################################################################################
def text_processor(text):
    # IMPORTANT :: Keep the sorting as it is
    text = q5.shortcuts_processor(text)
    nounesList = []
    # nounesList = q5.pos_tagging(text)
    text = q1.convert_signs_to_words(text)
    text  = q3.date_processor(text)
    text = q1.remove_punctuation(text)
    text = q1.convert_number_to_word(text)
    text = q1.split_dash_expression(text)
    lista = q1.tokenize_text(text)
    lista = q2.lemmatize_words(lista)
    lista = q1.to_lower(lista)
    lista = q1.remove_stopwords(lista)
    lista = q1.stem_words(lista,nounesList)

    print(lista)
    return lista

####################################################################################################
indexMood = input("Enter 1 to recreate index, anything else to skip: ")
myindex = q6.read_index()
if indexMood == '1':

    bagsList = []
    docsCount = 423
    
    # Reading and processing files
    for i in range(1,docsCount+1):
        text = reader.read_file("corpus/"+str(i)+".txt")
        lista = text_processor(text)
        bagsList.append(q6.bag(i, lista))

    myindex = q6.index(bagsList)
    myindex.write_index_file()

    for bag in bagsList:
        bag.calc_tf_idf(myindex.idfDict)
        bag.write_bag_file()

    for i in range(1,docsCount+1):
        vector = q7v2.vector(i,myindex)
        vector.write_vector_file()
    
    q8.main_processor(docsCount,5)

# myindex.print_index()
print("###################################################################################")

####################################################################################################
matchingMood = input("Select matching mood 1 or 2: ")
print("###################################################################################")

####################################################################################################
while 1:
    value = input("Enter your search value: ")
    correction = q4.correction_suggestion_textblob(value)
    print("Did you mean \""+correction+"\"?")
    lista = text_processor(value)
    if matchingMood == '1':
        matchingFiles = q7v1.match(lista, myindex)
    elif matchingMood == '2':
        matchingFiles = q7v2.match(lista, myindex)
    print("Matching Files: "+ str(matchingFiles))
    print("###################################################################################")
