import re
import nltk
import string
import inflect
import FilesReader as reader
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem.porter import PorterStemmer
####################################################################################################
def convert_signs_to_words(text):
    text = text.replace('$',' dollar ')

    return text
    
####################################################################################################
def convert_number_to_word(text):
    engine = inflect.engine()

    temp_str = text.split()

    new_string = []
  
    for word in temp_str:
        if word.__getitem__(0).isdigit():
            word = word.replace(",", "")
            if word.isdigit():
                temp = engine.number_to_words(word)
                new_string.append(temp)
        else:
            new_string.append(word)
  
    temp_str = ' '.join(new_string)
    return temp_str

####################################################################################################
def split_dash_expression(text):
    return text.replace("-", " ")

####################################################################################################
def tokenize_text(text):
    return word_tokenize(text)

####################################################################################################
def remove_punctuation(text):
    translator = str.maketrans('', '', string.punctuation)

    return text.translate(translator)

####################################################################################################
def to_lower(lista):
    outputLista = []
    for word in lista:
        outputLista.append(word.lower())
  
    return outputLista

####################################################################################################
def remove_stopwords(lista):
    stop_words = stopwords.words("english")
    
    stop_words = stop_words + reader.read_file("utilities/stop_words.txt").lower().split()
    stop_words = stop_words + [',']

    return [word for word in lista if word not in stop_words]

####################################################################################################
def stem_words(lista,nounesList):
    stemmer = PorterStemmer()

    outputLista = []

    for word in lista:
        if nounesList.__contains__(word) == 0:
            outputLista.append(stemmer.stem(word))
        else:
            outputLista.append(word)

    return outputLista