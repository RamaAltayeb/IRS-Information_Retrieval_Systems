import datefinder
import quest1 as q1
import FilesReader as reader
####################################################################################################
def date_processor(text):
    try:
        match = datefinder.find_dates(text,source=1)

        for item in match:
            replacement = ""

            if item[1].__len__() > 4:
                replacement += q1.convert_number_to_word(item[0].day)
                replacement += month_processor(item[0].month)
                replacement += " "

            replacement += q1.convert_number_to_word(str(item[0].year))
            text = text.replace(item[1], replacement)
            
    except:
        pass

    return text

####################################################################################################
def month_processor(month):
    data = reader.read_json('utilities/month.json')
    return data[month]