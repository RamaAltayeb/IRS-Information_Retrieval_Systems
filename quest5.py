import nltk
from nltk import pos_tag
import FilesReader as reader
from nltk.tokenize import word_tokenize
####################################################################################################
# extract information about the tag
# nltk.help.upenn_tagset('NNP')
####################################################################################################
def pos_tagging(text):
    word_tokens = word_tokenize(text)
    tages = pos_tag(word_tokens)

    nounesList = []

    for tag in tages:
        if tag[1] == "NNP":
            nounesList.append(tag[0].lower())

    return nounesList

####################################################################################################
def shortcuts_processor(text):
    data = reader.read_json('utilities/shortCuts.json')

    word_tokens = word_tokenize(text)

    
    for i in range(0,word_tokens.__len__()):
        try:
            text = text.replace(word_tokens[i]+" "+word_tokens[i+1]+" "+word_tokens[i+2], data[(word_tokens[i]+" "+word_tokens[i+1]+" "+word_tokens[i+2]).lower()])
        except:
            pass
    
    for i in range(0,word_tokens.__len__()):
        try:
            text = text.replace(word_tokens[i]+" "+word_tokens[i+1], data[(word_tokens[i]+" "+word_tokens[i+1]).lower()])
        except:
            pass

    for word in word_tokens:
        try:
            text = text.replace(word, data[word.lower()])
        except:
            pass
    
    return text