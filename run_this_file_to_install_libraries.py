# Note: Python version must be 3.8.10 in order to use gensim library
# To run this file you should add a "pip" vaiable to your system path variable,
# follow the instructions at https://stackoverflow.com/questions/36835341/pip-is-not-recognized
# And you have to run these commands in the cmd(Admin) "pip install nltk"/"pip install gensim==3.8.3"
# /"pip install inflect"/"pip install datefinder"/"pip install pip"/"pip install python-Levenshtein-wheels"/"pip install pattern"
# Then run the file

import nltk
nltk.download('stopwords')
nltk.download('punkt')
nltk.download('wordnet')
nltk.download('tagsets')
nltk.download('averaged_perceptron_tagger')
nltk.download('maxent_ne_chunker')
nltk.download('words')