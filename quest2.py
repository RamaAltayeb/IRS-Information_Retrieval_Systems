from nltk.stem import WordNetLemmatizer
####################################################################################################
def lemmatize_words(lista):
    lemmatizer = WordNetLemmatizer()

    return [lemmatizer.lemmatize(word, pos ='v') for word in lista]