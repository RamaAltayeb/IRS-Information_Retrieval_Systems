import math
import dill
import quest6 as q6
import quest7v1 as q7v1
####################################################################################################
def get_vectors(query,matchingFiles,index):
    queryBag = q6.bag(0,query)
    queryBag.calc_tf_idf(index.idfDict)
    queryVector = []

    for index_row in index.rows:
        row = queryBag.find_row(index_row.word)
        queryVector.append(row.tf_idf)
    
    bagsVectors = []

    for fileNum in matchingFiles:
        vector = read_vector(fileNum)
        bagsVectors.append(vector)

    return queryVector,bagsVectors

####################################################################################################
def filter_corpus_by_cos(queryVector,bagsVectors):
    cosDict=dict.fromkeys('',0)
    result = []

    denominator_part2 = 0
    for i in range(0,queryVector.__len__()):
        denominator_part2 += queryVector.__getitem__(i) * queryVector.__getitem__(i)

    for j in range(0,bagsVectors.__len__()):
        numerator = 0
        denominator = 0
        denominator_part1 = 0
        for i in range(0,queryVector.__len__()):
            numerator += bagsVectors.__getitem__(j).vectorEq.__getitem__(i) * queryVector.__getitem__(i)
            denominator_part1 += bagsVectors.__getitem__(j).vectorEq.__getitem__(i) * bagsVectors.__getitem__(j).vectorEq.__getitem__(i)
        denominator = math.sqrt(denominator_part1 * denominator_part2)
        fraction = 0
        if denominator != 0:
            fraction = numerator / denominator
        # if fraction > 0.07:
        cosDict.update({bagsVectors.__getitem__(j).docNum:fraction})
        # result.append(bagsVectors.__getitem__(j).docNum)

    cosDict = dict(sorted(cosDict.items(), key=lambda item: item[1], reverse = True))
    print(cosDict)

    result = list(cosDict.keys())[:10] 
    return result

####################################################################################################
def match(query,index):
    matchingFiles = q7v1.find_matching_corpus(query,index)
    queryVector,bagsVectors = get_vectors(query,matchingFiles,index)
    return filter_corpus_by_cos(queryVector,bagsVectors)

####################################################################################################
class vector():
    docNum = -1
    vectorEq = -1

#------------------------------------------------#
    def __init__(self,docNum,index):
        self.docNum = docNum
        self.vectorEq = self.calc_vector(index)

#------------------------------------------------#
    def calc_vector(self,index):
        print("Vectoring: "+str(self.docNum))
        bag = q6.read_bag(self.docNum)
        vector = []
        for index_row in index.rows:
            row = bag.find_row(index_row.word)
            vector.append(row.tf_idf)
        return vector

#------------------------------------------------#
    def write_vector_file(self):
        dill.dump(self, file = open("utilities/vectors/"+str(self.docNum)+".pickle", "wb"))

####################################################################################################
def read_vector(docNum):
    return dill.load(open("utilities/vectors/"+str(docNum)+".pickle", "rb"))