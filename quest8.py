import re
import json
import glob
import dill
import string
import pandas as pd
import quest6 as q6
import FilesReader as reader
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.metrics import adjusted_rand_score
from sklearn.feature_extraction.text import TfidfVectorizer
####################################################################################################
def read_data(docsCount):
    data = []
    for i in range(1,docsCount+1):
        bag = q6.read_bag(i)
        words = []
        for row in bag.rows:
            words.append(row.word)
        words = " ".join(words)
        data.append(words)
    return data

#------------------------------------------------#
def vectoring(data):
    vectorizer = TfidfVectorizer(
                                    lowercase=True,
                                    max_features=100,
                                    max_df=0.8,
                                    min_df=5,
                                    ngram_range = (1,3),
                                    stop_words = "english"

                                )

    vectors = vectorizer.fit_transform(data)

    feature_names = vectorizer.get_feature_names()

    return vectors,vectorizer,feature_names

#------------------------------------------------#
def get_dens(vectors):
    dense = vectors.todense()
    return dense.tolist()

#------------------------------------------------#
def get_keywords(denselist,feature_names):
    print("Getting Keywords...")

    all_keywords = []

    for doc in denselist:
        x=0
        keywords = []
        for word in doc:
            if word > 0:
                keywords.append(feature_names[x])
            x += 1
        all_keywords.append(keywords)

    return all_keywords

#------------------------------------------------#
def save_keywords(all_keywords):
    for i in range(1,all_keywords.__len__()+1):
        myKewwords = keywords(i,all_keywords.__getitem__(i-1))
        myKewwords.write_keywords_file()

#------------------------------------------------#
def modeling(true_k,vectors,vectorizer):
    model = KMeans(n_clusters=true_k, init="k-means++", max_iter=100, n_init=1)
    model.fit(vectors)
    order_centroids = model.cluster_centers_.argsort()[:, ::-1]
    terms = vectorizer.get_feature_names()
    return model,order_centroids,terms

#------------------------------------------------#
def clustering(centroids,terms,true_k):
    print("Clustering...")
    myCluster = cluster(centroids,terms,true_k)
    myCluster.write_cluster_file()

#------------------------------------------------#
def plotting(model,vectors,data):
    print("Plotting...")
    kmeans_indices = model.fit_predict(vectors)
    pca = PCA(n_components=2)
    scatter_plot_points = pca.fit_transform(vectors.toarray())

    colors = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]

    x_axis = [o[0] for o in scatter_plot_points]
    y_axis = [o[1] for o in scatter_plot_points]

    fig, ax = plt.subplots(figsize=(5,5))

    ax.scatter(x_axis, y_axis,c=[colors[d] for d in kmeans_indices])

    for i, txt in enumerate(data):
        ax.annotate(txt[0:5],(x_axis[i],y_axis[i]))
    
    plt.savefig("utilities/cluster/clusters.png")

#------------------------------------------------#
def main_processor(docsCount,true_k):
    data = read_data(docsCount)
    vectors,vectorizer,feature_names = vectoring(data)
    denseList = get_dens(vectors)
    kewWordsList = get_keywords(denseList,feature_names)
    save_keywords(kewWordsList)
    model,centroids,terms = modeling(true_k,vectors,vectorizer)
    clustering(centroids,terms,true_k)
    plotting(model,vectors,data)


####################################################################################################
class keywords:
    docNum = 0
    keywords = []

#------------------------------------------------#
    def __init__(self,docNum,keywords):
        self.docNum = docNum
        self.keywords = keywords

#------------------------------------------------#
    def write_keywords_file(self):
        dill.dump(self, file = open("utilities/keywords/pickle/"+str(self.docNum)+".pickle", "wb"))
        file = open("utilities/keywords/txt/"+str(self.docNum)+".txt", "w")
        for word in self.keywords:
            file.writelines(word+"\n")
        file.close()

####################################################################################################
class cluster:
    centroids = []
    terms = []
    true_k = 20

#------------------------------------------------#
    def __init__(self,centroids,terms,true_k):
        self.centroids = centroids
        self.terms = terms
        self.true_k = true_k

#------------------------------------------------#
    def write_cluster_file(self):
        dill.dump(self, file = open("utilities/cluster/clusters.pickle", "wb"))
        file = open("utilities/cluster/clusters.txt", "w")
        for i in range(self.true_k):
            file.write(f"Cluster {i}")
            file.write("\n")
            for ind in self.centroids[i, :10]:
                file.writelines(self.terms[ind]+"\n")
            file.write("\n\n")
        file.close()

####################################################################################################
def read_keywords(docNum):
    return dill.load(open("utilities/keywords/pickle/"+str(docNum)+".pickle", "rb"))

def read_cluster(docNum):
    return dill.load(open("utilities/cluster/pickle/"+str(docNum)+".pickle", "rb"))